package com.bryenvieira.lushtechnicaltest.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.bryenvieira.lushtechnicaltest.repository.SpaceXRepository
import com.bryenvieira.lushtechnicaltest.repository.model.SpaceXLaunch
import com.bryenvieira.lushtechnicaltest.repository.model.SpaceXLaunchLinks
import com.bryenvieira.lushtechnicaltest.repository.model.SpaceXLaunchPatch
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.timeout
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.*
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.util.*

@RunWith(JUnit4::class)
class MainActivityViewModelTest {
    private lateinit var viewModel: MainActivityViewModel
    private lateinit var repository: SpaceXRepository
    private lateinit var observer: Observer<List<SpaceXLaunch>>

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @ObsoleteCoroutinesApi
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @ExperimentalCoroutinesApi
    @ObsoleteCoroutinesApi
    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
        repository = mock()
        viewModel = MainActivityViewModel(repository)
        observer = mock()
    }

    @ObsoleteCoroutinesApi
    @ExperimentalCoroutinesApi
    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `when getLatestLaunches returns valid response, then observer is updated with success`() {
        runBlocking {
            whenever(repository.getLatestLaunches()).thenReturn(validResponse)
        }
        runBlocking {
            viewModel.latestLaunches.observeForever(observer)
            delay(DEFAULT_TIMEOUT)
            verify(repository).getLatestLaunches()
            verify(observer, timeout(DEFAULT_TIMEOUT)).onChanged(validResponse)
        }
    }

    @Test
    fun `when getLatestLaunches returns invalid but not null response, then observer is updated with failure`() {
        runBlocking {
            whenever(repository.getLatestLaunches()).thenReturn(invalidResponse)
        }
        runBlocking {
            viewModel.latestLaunches.observeForever(observer)
            delay(DEFAULT_TIMEOUT)
            verify(repository).getLatestLaunches()
            verify(observer, timeout(DEFAULT_TIMEOUT)).onChanged(invalidResponse)
        }
    }

    @Test
    fun `when getLatestLaunches returns null, then observer is updated with failure`() {
        runBlocking {
            whenever(repository.getLatestLaunches()).thenReturn(nullResponse)
        }
        runBlocking {
            viewModel.latestLaunches.observeForever(observer)
            delay(DEFAULT_TIMEOUT)
            verify(repository).getLatestLaunches()
            verify(observer, timeout(DEFAULT_TIMEOUT)).onChanged(nullResponse)
        }
    }

    private val validResponse = listOf(
        SpaceXLaunch(
            "5eb87d46ffd86e000604b388",
            "CCtCap Demo Mission 2",
            true,
            Date(),
            SpaceXLaunchLinks(
                SpaceXLaunchPatch(
                    "https://images2.imgbox.com/ab/79/Wyc9K7fv_o.png",
                    "https://images2.imgbox.com/eb/0f/Vev7xkUX_o.png"
                )
            )
        )
    )
    private val invalidResponse =
        listOf(SpaceXLaunch("", "", false, Date(), SpaceXLaunchLinks(SpaceXLaunchPatch("", ""))))
    private val nullResponse = null
}

private const val DEFAULT_TIMEOUT = 50L
