package com.bryenvieira.lushtechnicaltest.repository

import com.bryenvieira.lushtechnicaltest.repository.api.SpaceXApi
import com.bryenvieira.lushtechnicaltest.repository.model.SpaceXLaunch
import com.bryenvieira.lushtechnicaltest.repository.model.SpaceXLaunchLinks
import com.bryenvieira.lushtechnicaltest.repository.model.SpaceXLaunchPatch
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.util.*

@RunWith(JUnit4::class)
class SpaceXRepositoryTest {
    private lateinit var api: SpaceXApi
    private lateinit var repository: SpaceXRepository

    @Before
    fun setUp() {
        api = mock()
        runBlocking {
            whenever(api.getLatestLaunches()).thenReturn(validResponse)
        }
        repository = SpaceXRepository(api)
    }

    @Test
    fun `when getLatestLaunches is called, then data is returned`() =
        /* This is a pretty useless test right now because we don't do anything interesting with
         the API right now, but nonetheless, it's useful to have this boilerplate for later */
        runBlocking {
            assertEquals(validResponse, repository.getLatestLaunches())
        }

    private val validResponse = listOf(
        SpaceXLaunch(
            "5eb87d46ffd86e000604b388",
            "CCtCap Demo Mission 2",
            true,
            Date(),
            SpaceXLaunchLinks(
                SpaceXLaunchPatch(
                    "https://images2.imgbox.com/ab/79/Wyc9K7fv_o.png",
                    "https://images2.imgbox.com/eb/0f/Vev7xkUX_o.png"
                )
            )
        )
    )
}