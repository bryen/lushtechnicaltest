package com.bryenvieira.lushtechnicaltest.ui

import android.os.Bundle
import android.view.View
import androidx.activity.ComponentActivity
import androidx.lifecycle.Observer
import com.bryenvieira.lushtechnicaltest.databinding.MainActivityBinding
import com.bryenvieira.lushtechnicaltest.repository.model.SpaceXLaunch
import com.bryenvieira.lushtechnicaltest.ui.viewmodel.MainActivityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : ComponentActivity() {
    private val viewModel: MainActivityViewModel by viewModel()
    private val observer = Observer<List<SpaceXLaunch>> {
        updateUi(it)
    }
    private lateinit var binding: MainActivityBinding
    private lateinit var adapter: SpaceXLaunchRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        binding.progressCircular.visibility = View.VISIBLE

        adapter = SpaceXLaunchRecyclerViewAdapter(emptyList(), this@MainActivity)
        binding.spacexLaunchesRv.adapter = adapter

        viewModel.latestLaunches.observe(this, observer)
    }

    private fun updateUi(models: List<SpaceXLaunch>) {
        binding.progressCircular.visibility = View.GONE
        binding.spacexLaunchesRv.visibility = View.VISIBLE
        adapter.setData(models)
    }
}
