package com.bryenvieira.lushtechnicaltest.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.bryenvieira.lushtechnicaltest.repository.SpaceXRepository
import com.bryenvieira.lushtechnicaltest.repository.model.SpaceXLaunch
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { MainActivityViewModel(get()) }
}

class MainActivityViewModel(private val repository: SpaceXRepository) :
    ViewModel() {

    val latestLaunches: LiveData<List<SpaceXLaunch>> = liveData {
        emit(repository.getLatestLaunches())
    }
}
