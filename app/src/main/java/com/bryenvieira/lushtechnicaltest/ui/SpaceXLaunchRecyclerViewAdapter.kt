package com.bryenvieira.lushtechnicaltest.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.RecyclerView
import com.bryenvieira.lushtechnicaltest.R
import com.bryenvieira.lushtechnicaltest.databinding.LaunchListItemBinding
import com.bryenvieira.lushtechnicaltest.repository.model.SpaceXLaunch
import com.bumptech.glide.Glide
import org.joda.time.DateTime


class SpaceXLaunchRecyclerViewAdapter(
    private var models: List<SpaceXLaunch>,
    private val context: Context
) :
    RecyclerView.Adapter<SpaceXLaunchRecyclerViewAdapter.SpaceXLaunchViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpaceXLaunchViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = LaunchListItemBinding.inflate(inflater)
        return SpaceXLaunchViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SpaceXLaunchViewHolder, position: Int) {
        holder.bind(models[position])
    }

    override fun getItemCount() = models.size

    fun setData(data: List<SpaceXLaunch>) {
        models = data
        notifyDataSetChanged()
    }

    inner class SpaceXLaunchViewHolder(private val binding: LaunchListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(model: SpaceXLaunch) {
            with(model) {
                Glide.with(context).load(links.patch.small).into(binding.launchImage)
                binding.launchName.text = if (name.isNotEmpty()) name else "Unknown"
                binding.launchDate.text = DateTime(launchDate).toLocalDate().toString()
                val successDrawable = AppCompatResources.getDrawable(
                    context,
                    if (wasSuccessful) R.drawable.ic_tick else R.drawable.ic_cross
                )
                binding.launchSuccess.setCompoundDrawables(null, null, successDrawable, null)
            }
        }
    }
}
