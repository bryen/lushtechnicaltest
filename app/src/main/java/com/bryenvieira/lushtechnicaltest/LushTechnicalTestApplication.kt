package com.bryenvieira.lushtechnicaltest

import android.app.Application
import androidx.startup.AppInitializer
import com.bryenvieira.lushtechnicaltest.repository.SpaceXRepositoryModule
import com.bryenvieira.lushtechnicaltest.repository.api.networkModule
import com.bryenvieira.lushtechnicaltest.ui.viewmodel.viewModelModule
import net.danlew.android.joda.JodaTimeInitializer
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class LushTechnicalTestApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        AppInitializer.getInstance(this).initializeComponent(JodaTimeInitializer::class.java)

        startKoin {
            androidLogger()
            androidContext(this@LushTechnicalTestApplication)
            modules(
                listOf(
                    viewModelModule, networkModule, SpaceXRepositoryModule
                )
            )
        }
    }
}
