package com.bryenvieira.lushtechnicaltest.repository.api

import com.bryenvieira.lushtechnicaltest.repository.model.SpaceXLaunch
import retrofit2.http.GET

interface SpaceXApi {
    @GET("launches")
    suspend fun getLatestLaunches(): List<SpaceXLaunch>
}
