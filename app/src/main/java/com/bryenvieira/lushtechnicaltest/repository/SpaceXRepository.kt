package com.bryenvieira.lushtechnicaltest.repository

import com.bryenvieira.lushtechnicaltest.repository.api.SpaceXApi
import org.koin.dsl.module

val SpaceXRepositoryModule = module {
    factory { SpaceXRepository(get()) }
}

class SpaceXRepository(private val spaceXApi: SpaceXApi) {
    suspend fun getLatestLaunches() = spaceXApi.getLatestLaunches()
}
