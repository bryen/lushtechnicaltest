package com.bryenvieira.lushtechnicaltest.repository.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class SpaceXLaunch(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("success") val wasSuccessful: Boolean,
    @SerializedName("static_fire_date_utc") val launchDate: Date,
    @SerializedName("links") val links: SpaceXLaunchLinks,
)

data class SpaceXLaunchLinks(
    val patch: SpaceXLaunchPatch
)

data class SpaceXLaunchPatch(
    val small: String,
    val large: String
)
